---
defaults:
- 'false'
flags:
- dot
minimums: []
title: compound
types:
- bool
used_by: G
---
If true, allow edges between clusters.

See [`lhead`]({{< ref "lhead.md" >}}) and [`ltail`]({{< ref "ltail.md" >}}).
