---
defaults:
- '14.0'
flags: []
minimums:
- '1.0'
title: fontsize
types:
- double
used_by: ENGC
---
Font size, [in points]({{< ref "_index.md#points" >}}), used for text.
