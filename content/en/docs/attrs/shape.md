---
defaults:
- ellipse
flags: []
minimums: []
title: shape
types:
- shape
used_by: "N"
---
Sets the [shape]({{< ref "../shapes.md" >}}) of a node.
